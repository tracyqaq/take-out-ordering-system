package com.tracy.reggie_take_out.common;

/**
 * 基于ThreadLocal封装工具类，用于保存和获取当前登录用户的id
 */
public class BaseContext {
    private static ThreadLocal<Long> threadLocal = new ThreadLocal<>();

    // 设置当前登录用户的id
    public static void setCurrentEmpId(Long id) {
        threadLocal.set(id);
    }

    // 读取当前登录用户的id
    public static Long getCurrentEmpId() {
        return threadLocal.get();
    }
}
