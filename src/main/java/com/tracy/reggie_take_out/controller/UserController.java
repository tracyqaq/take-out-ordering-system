package com.tracy.reggie_take_out.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.tracy.reggie_take_out.common.R;
import com.tracy.reggie_take_out.entity.User;
import com.tracy.reggie_take_out.service.UserService;
import com.tracy.reggie_take_out.utils.SendEmailUtils;
import com.tracy.reggie_take_out.utils.ValidateCodeUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

@Slf4j
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private SendEmailUtils sendEmailUtils;

    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 发送验证码
     *
     * @param user
     * @param session
     * @return
     */
    @PostMapping("/sendMsg")
    public R<String> sendMsg(@RequestBody User user, HttpSession session) {
        // 获取邮箱
        String phone = user.getPhone();
        if (StringUtils.isNotEmpty(phone)) {
            // 生成随机验证码
            String code = ValidateCodeUtils.generateValidateCode(4).toString();
            log.info("code = {}", code);
            // 发送邮件
            sendEmailUtils.sendCode(phone, code);


//            // 将生成的验证码保存到session
//            session.setAttribute(phone, code);
//            // 设置验证码2分钟失效
//            final Timer timer = new Timer();
//            timer.schedule(new TimerTask() {
//                @Override
//                public void run() {
//                    session.removeAttribute(phone);
//                    timer.cancel();
//                }
//            }, 2 * 60 * 1000);

            // 将生成的验证码保存到Redis中，设置有效期为2分钟 key:邮箱 value:验证码
            redisTemplate.opsForValue().set(phone, code, 2, TimeUnit.MINUTES);

            return R.success("验证码发送成功");
        }
        return R.error("验证码发送失败");
    }

    /**
     * 移动端用户登录
     *
     * @param map
     * @param session
     * @return
     */
    @PostMapping("/login")
    public R<User> login(@RequestBody Map<String, String> map, HttpSession session) {
        log.info(map.toString());
        // 获取邮箱
        String phone = map.get("phone");
        // 获取用户输入的验证码
        String code = map.get("code");

//        // 获取session中保存的验证码
//        Object codeInSession = session.getAttribute(phone);

        // 获取Redis中保存的验证码
        Object codeInSession = redisTemplate.opsForValue().get(phone);

        // 验证码比对
        if (codeInSession != null && codeInSession.equals(code)) {
            // 比对成功
            // 判断当前邮箱是否为新用户，如果是新用户则自动注册
            LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(User::getPhone, phone);
            User user = userService.getOne(queryWrapper);
            // 新用户，自动注册
            if (user == null) {
                user = new User();
                user.setPhone(phone);
                user.setStatus(1);
                userService.save(user);
            }
            // 登录成功，将userId存入session并删除Redis中保存的验证码
            session.setAttribute("user", user.getId());
            redisTemplate.delete(phone);

            return R.success(user);
        }
        return R.error("登录失败");
    }

    /**
     * 用户退出
     *
     * @param request
     * @return
     */
    @PostMapping("/logout")
    public R<String> logout(HttpServletRequest request) {
        // 清理Session中保存的当前用户id
        request.getSession().removeAttribute("user");
        return R.success("退出成功");
    }
}
