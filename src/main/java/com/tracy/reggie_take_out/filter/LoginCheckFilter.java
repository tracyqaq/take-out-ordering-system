package com.tracy.reggie_take_out.filter;

import com.alibaba.fastjson.JSON;
import com.tracy.reggie_take_out.common.BaseContext;
import com.tracy.reggie_take_out.common.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.AntPathMatcher;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 检查用户是否已经登录
 */

@WebFilter(filterName = "loginCheckFilter", urlPatterns = "/*")
@Slf4j
public class LoginCheckFilter implements Filter {
    // 路径匹配器，支持通配符
    public static final AntPathMatcher PATH_MATCHER = new AntPathMatcher();

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        // 定义不需要处理的请求路径(登录、登出、前端资源)
        String[] urls = new String[]{
                "/employee/login",
                "/employee/logout",
                "/backend/**",
                "/front/**",
                "/common/**",
                "/user/sendMsg",  // 发送验证码
                "/user/login",   // 移动端登录
                "/user/logout"   // 移动端退出
        };
        // 1、获取本次请求的URI
        String requestURI = request.getRequestURI();
        log.info("拦截到请求：{}", requestURI);
        // 2、判断本次请求是否需要处理
        boolean check = check(urls, requestURI);
        // 3、如果不需要处理则放行
        if (check) {
            log.info("本次请求：{}，不需要处理", requestURI);
            filterChain.doFilter(request, response);
            return;
        }
        // 4-1、判断登录状态，若已经登录则放行（后台）
        if (request.getSession().getAttribute("employee") != null) {
            Long empId = (Long) request.getSession().getAttribute("employee");
            log.info("用户已登录，id: {}", empId);
            BaseContext.setCurrentEmpId(empId);    // 将当前登录用户的id放进ThreadLocal
            filterChain.doFilter(request, response);
            return;
        }

        // 4-2、判断登录状态，若已经登录则放行（移动端）
        if (request.getSession().getAttribute("user") != null) {
            Long userId = (Long) request.getSession().getAttribute("user");
            log.info("用户已登录，id: {}", userId);
            BaseContext.setCurrentEmpId(userId);    // 将当前登录用户的id放进ThreadLocal
            filterChain.doFilter(request, response);
            return;
        }

        // 5、如果未登录则返回未登录状态，通过输出流向前端页面响应数据
        log.info("用户未登录");
        response.getWriter().write(JSON.toJSONString(R.error("Not Login")));
    }

    /**
     * 路径匹配，判断本次请求是否可以放行
     *
     * @param urls
     * @param requestURI
     * @return
     */
    public boolean check(String[] urls, String requestURI) {
        for (String url : urls) {
            boolean match = PATH_MATCHER.match(url, requestURI);
            if (match)  // 放行
                return true;
        }
        return false;
    }
}
