package com.tracy.reggie_take_out.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tracy.reggie_take_out.dto.DishDto;
import com.tracy.reggie_take_out.entity.Dish;

import java.util.List;

public interface DishService extends IService<Dish> {
    // 新增菜品和对应的口味，需要操作两张表：dish、dish_flavor
    void saveWithFlavor(DishDto dishDto);

    // 根据菜品id查询菜品信息和对应口味信息
    DishDto getByIdWithFlavor(Long id);

    // 提交更新
    void updateWithFlavor(DishDto dishDto);

    // 更新菜品状态
    void updateStatusBatch(Integer status, String[] ids);

    // 根据id删除菜品
    void deleteDishBatch(List<Long> ids);
}
