package com.tracy.reggie_take_out.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tracy.reggie_take_out.entity.OrderDetail;

public interface OrderDetailService extends IService<OrderDetail> {
}
