package com.tracy.reggie_take_out.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tracy.reggie_take_out.entity.Orders;

public interface OrderService extends IService<Orders> {
    // 用户下单
    void submit(Orders orders);
}
