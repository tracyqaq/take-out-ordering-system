package com.tracy.reggie_take_out.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tracy.reggie_take_out.entity.SetmealDish;

public interface SetmealDishService extends IService<SetmealDish> {
}
