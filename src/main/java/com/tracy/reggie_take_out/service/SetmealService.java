package com.tracy.reggie_take_out.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tracy.reggie_take_out.dto.SetmealDto;
import com.tracy.reggie_take_out.entity.Setmeal;

import java.util.List;


public interface SetmealService extends IService<Setmeal> {

    // 新增套餐，同时保存套餐和菜品的关联关系
    void saveWithDish(SetmealDto setmealDto);

    // 删除套餐，同时删除对应setmeal_dish记录
    void removeWithDish(List<Long> ids);

    // 修改套餐状态
    void updateStatusBatch(Integer status, String[] ids);

    // 根据套餐id查询setmeal表套餐信息和setmeal_dish表对应套餐信息
    SetmealDto getByIdWithDish(Long id);

    // 提交更新
    void updateWithDish(SetmealDto setmealDto);
}
