package com.tracy.reggie_take_out.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tracy.reggie_take_out.common.CustomException;
import com.tracy.reggie_take_out.entity.Category;
import com.tracy.reggie_take_out.entity.Dish;
import com.tracy.reggie_take_out.entity.Setmeal;
import com.tracy.reggie_take_out.mapper.CategoryMapper;
import com.tracy.reggie_take_out.service.CategoryService;
import com.tracy.reggie_take_out.service.DishService;
import com.tracy.reggie_take_out.service.SetmealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, Category> implements CategoryService {
    @Autowired
    private DishService dishService;

    @Autowired
    private SetmealService setmealService;

    /**
     * 根据id删除分类，删除前需要判断
     *
     * @param id
     */
    @Override
    public void remove(Long id) {
        // 查询当前分类是否关联菜品，若关联则抛出异常
        LambdaQueryWrapper<Dish> dishLambdaQueryWrapper = new LambdaQueryWrapper<>();
        // 添加查询条件，根据id查询
        dishLambdaQueryWrapper.eq(Dish::getCategoryId, id);
        int countDish = dishService.count(dishLambdaQueryWrapper);
        // 关联了菜品，抛出业务异常
        if (countDish > 0)
            throw new CustomException("当前分类关联了菜品，无法删除");

        // 查询当前分类是否关联套餐，若关联则抛出异常
        LambdaQueryWrapper<Setmeal> setmealLambdaQueryWrapper = new LambdaQueryWrapper<>();
        // 添加查询条件，根据id查询
        setmealLambdaQueryWrapper.eq(Setmeal::getCategoryId, id);
        int countSetmeal = setmealService.count(setmealLambdaQueryWrapper);
        // 关联了套餐，抛出业务异常
        if (countSetmeal > 0)
            throw new CustomException("当前分类关联了套餐，无法删除");

        // 正常删除分类
        this.removeById(id);
    }
}
