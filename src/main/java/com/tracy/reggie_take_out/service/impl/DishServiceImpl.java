package com.tracy.reggie_take_out.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tracy.reggie_take_out.common.CustomException;
import com.tracy.reggie_take_out.dto.DishDto;
import com.tracy.reggie_take_out.entity.Dish;
import com.tracy.reggie_take_out.entity.DishFlavor;
import com.tracy.reggie_take_out.entity.Setmeal;
import com.tracy.reggie_take_out.entity.SetmealDish;
import com.tracy.reggie_take_out.mapper.DishMapper;
import com.tracy.reggie_take_out.service.DishFlavorService;
import com.tracy.reggie_take_out.service.DishService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class DishServiceImpl extends ServiceImpl<DishMapper, Dish> implements DishService {
    @Autowired
    private DishFlavorService dishFlavorService;

    // 新增菜品和对应的口味，需要操作两张表：dish、dish_flavor
    @Transactional // 开启事务
    @Override
    public void saveWithFlavor(DishDto dishDto) {
        // 保存菜品的基本信息到菜品表dish
        this.save(dishDto);

        Long dishId = dishDto.getId();
        // 菜品口味
        List<DishFlavor> flavors = dishDto.getFlavors();
        // 遍历从前端传来的DishFlavor集合，将其中的每一项的dishId设置为当前菜品的id
        flavors = flavors.stream().map(item -> {
            item.setDishId(dishId);
            return item;
        }).collect(Collectors.toList());
        // 保存菜品口味数据到菜品口味表dish_flavor
        dishFlavorService.saveBatch(flavors);
    }

    // 根据菜品id查询菜品信息和对应口味信息
    @Override
    public DishDto getByIdWithFlavor(Long id) {
        // 从dish表查询菜品基本信息，将对应字段复制给dishDto
        Dish dish = this.getById(id);
        DishDto dishDto = new DishDto();
        BeanUtils.copyProperties(dish, dishDto);
        // 从dish_flavor表查询当前菜品对应口味信息
        LambdaQueryWrapper<DishFlavor> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(DishFlavor::getDishId, dish.getId());
        List<DishFlavor> flavors = dishFlavorService.list(queryWrapper);
        dishDto.setFlavors(flavors);
        return dishDto;
    }

    // 提交更新
    @Transactional
    @Override
    public void updateWithFlavor(DishDto dishDto) {
        // 更新dish表基本信息
        this.updateById(dishDto);
        // 清除当前菜品对应的dish_flavor数据
        LambdaQueryWrapper<DishFlavor> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(DishFlavor::getDishId, dishDto.getId());
        dishFlavorService.remove(queryWrapper);
        // 添加当前提交的dish_flavor数据
        List<DishFlavor> flavors = dishDto.getFlavors();
        // 为flavors设置当前菜品的id
        flavors = flavors.stream().map(item -> {
            item.setDishId(dishDto.getId());
            return item;
        }).collect(Collectors.toList());
        dishFlavorService.saveBatch(flavors);
    }

    // 更新菜品状态
    @Override
    public void updateStatusBatch(Integer status, String[] ids) {
        for (String id : ids) {
            Dish dish = this.getById(id);
            dish.setStatus(status);
            this.updateById(dish);
        }
    }

    // 根据id删除菜品
    @Transactional
    @Override
    public void deleteDishBatch(List<Long> ids) {
        // 查询套餐状态，确定是否可以删除（停售不可删除或status=1）
        LambdaQueryWrapper<Dish> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(Dish::getId, ids);
        queryWrapper.eq(Dish::getStatus, 1);
        int count = this.count(queryWrapper);
        // 若不可删除，抛出一个业务异常
        if (count > 0)
            throw new CustomException("菜品正在售卖中，不可删除");
        // 若可删除，先删除setmeal表记录
        this.removeByIds(ids);
        // 删除对应dish_flavor表记录
        LambdaQueryWrapper<DishFlavor> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.in(DishFlavor::getDishId, ids);
        dishFlavorService.remove(lambdaQueryWrapper);
    }
}
