package com.tracy.reggie_take_out.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tracy.reggie_take_out.common.CustomException;
import com.tracy.reggie_take_out.dto.DishDto;
import com.tracy.reggie_take_out.dto.SetmealDto;
import com.tracy.reggie_take_out.entity.Dish;
import com.tracy.reggie_take_out.entity.DishFlavor;
import com.tracy.reggie_take_out.entity.Setmeal;
import com.tracy.reggie_take_out.entity.SetmealDish;
import com.tracy.reggie_take_out.mapper.SetmealMapper;
import com.tracy.reggie_take_out.service.SetmealDishService;
import com.tracy.reggie_take_out.service.SetmealService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SetmealServiceImpl extends ServiceImpl<SetmealMapper, Setmeal> implements SetmealService {
    @Autowired
    private SetmealDishService setmealDishService;

    // 新增套餐，同时保存套餐和菜品的关联关系
    @Transactional
    @Override
    public void saveWithDish(SetmealDto setmealDto) {
        // 保存套餐的基本信息（setmeal表）
        this.save(setmealDto);
        // 取出套餐中包含的套餐-菜品集合
        List<SetmealDish> setmealDishes = setmealDto.getSetmealDishes();
        // 遍历集合，为其套餐id赋值
        setmealDishes.stream().map(item -> {
            item.setSetmealId(setmealDto.getId());
            return item;
        }).collect(Collectors.toList());
        // 保存套餐和菜品的关联信息（setmieal_dish表）X
        setmealDishService.saveBatch(setmealDishes);
    }

    // 删除套餐，同时删除对应setmeal_dish记录
    @Transactional
    @Override
    public void removeWithDish(List<Long> ids) {
        // 查询套餐状态，确定是否可以删除（停售不可删除或status=1）
        LambdaQueryWrapper<Setmeal> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(Setmeal::getId, ids);
        queryWrapper.eq(Setmeal::getStatus, 1);
        int count = this.count(queryWrapper);
        // 若不可删除，抛出一个业务异常
        if (count > 0)
            throw new CustomException("套餐正在售卖中，不可删除");
        // 若可删除，先删除setmeal表记录
        this.removeByIds(ids);
        // 删除对应setmeal_dish表记录
        LambdaQueryWrapper<SetmealDish> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.in(SetmealDish::getSetmealId, ids);
        setmealDishService.remove(lambdaQueryWrapper);
    }

    // 修改套餐状态
    @Override
    public void updateStatusBatch(Integer status, String[] ids) {
        for (String id : ids) {
            Setmeal setmeal = this.getById(id);
            setmeal.setStatus(status);
            this.updateById(setmeal);
        }
    }

    // 根据套餐id查询setmeal表套餐信息和setmeal_dish表对应菜品信息
    @Override
    public SetmealDto getByIdWithDish(Long id) {
        // 从setmeal表查询套餐基本信息，将对应字段复制给setmealDto
        Setmeal setmeal = this.getById(id);
        SetmealDto setmealDto = new SetmealDto();
        BeanUtils.copyProperties(setmeal, setmealDto);
        // 从setmeal_dish表查询当前套餐对应菜品信息
        LambdaQueryWrapper<SetmealDish> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SetmealDish::getSetmealId, setmeal.getId());
        List<SetmealDish> setmealDishes = setmealDishService.list(queryWrapper);
        setmealDto.setSetmealDishes(setmealDishes);
        return setmealDto;
    }

    // 提交更新
    @Transactional
    @Override
    public void updateWithDish(SetmealDto setmealDto) {
        // 更新setmeal表基本信息
        this.updateById(setmealDto);
        // 清除当前套餐对应的setmeal_dish数据
        LambdaQueryWrapper<SetmealDish> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SetmealDish::getSetmealId, setmealDto.getId());
        setmealDishService.remove(queryWrapper);
        // 添加当前提交的setmeal_dish数据
        List<SetmealDish> setmealDishes = setmealDto.getSetmealDishes();
        // 为setmealDishes设置当前套餐的id
        setmealDishes = setmealDishes.stream().map(item -> {
            item.setSetmealId(setmealDto.getId());
            return item;
        }).collect(Collectors.toList());
        setmealDishService.saveBatch(setmealDishes);
    }
}
