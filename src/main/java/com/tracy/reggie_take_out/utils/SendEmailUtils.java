package com.tracy.reggie_take_out.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

@Component
public class SendEmailUtils {
    @Autowired
    private JavaMailSender sender;

    @Value("${spring.mail.username}")
    private String username;

    public void sendCode(String account, String code) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setSubject("【瑞吉外卖】验证消息"); // 发送邮件的标题
        message.setText("你正在进行登录操作，验证码：" + code + "，切勿将验证码泄露给他人，本条验证码有效期2分钟。"); // 发送邮件的内容
        message.setTo(account); // 登录用户的邮箱账号
        message.setFrom(username); // 发送邮件的邮箱账号，注意一定要和配置文件中的一致！
        sender.send(message); // 调用send方法发送邮件即可
    }
}
